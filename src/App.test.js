import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";

import App from "../src/components/TodoApp";
import TodoForm from "../src/components/TodoForm";
import TodoList from "../src/components/TodoList";

const dummyData = [
  {
    name: "Aliqua quis eu id nulla.",
    isComplete: false,
    id: 1
  },
  {
    name: "Aliquip fugiat irure nulla occaecat duis aute incididunt incididunt sint exercitation amet enim.",
    isComplete: false,
    id: 2
  },
  {
    name: "Adipisicing Lorem est labore mollit enim.",
    isComplete: false,
    id: 3
  }
];

it("submits", () => {
  const onSubmit = jest.fn();
  render(<TodoForm handleTodoSubmit={onSubmit} />);
  fireEvent.submit(screen.getByTestId("form"));
  expect(onSubmit).toHaveBeenCalled();
});

it("renders list", () => {
  render(<TodoList todos={dummyData} />);
  dummyData.forEach((todo) =>
    expect(screen.getByText(todo.name)).toBeInTheDocument()
  );
});

describe("<App /> test", () => {
  it("renders initial DOM", () => {
    render(<App />);
    expect(screen.getByTestId("input")).toBeInTheDocument();
    expect(screen.getByTestId("todo-list")).toBeInTheDocument();
    expect(screen.getByTestId("todo-count")).toBeInTheDocument();
  });

  // it("adds item", async () => {
  //   render(<App />);
  //   const value = "" + Math.random();
  //   const inputEl = screen.getByTestId("input");
  //   fireEvent.change(inputEl, { target: { value } });
  //   fireEvent.submit(inputEl);

  //   await waitFor(() => {
  //     expect(screen.getByTestId("input").value).toBe("");
  //     expect(screen.queryByText(value)).toBeInTheDocument();
  //   });
  // });
});
